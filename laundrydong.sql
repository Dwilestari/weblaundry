-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 22, 2018 at 11:49 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `laundrydong`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`nama`, `username`, `password`) VALUES
('admin', 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `kurir`
--

CREATE TABLE `kurir` (
  `kd_kurir` varchar(10) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `nm_kurir` varchar(50) NOT NULL,
  `no_plat` varchar(10) NOT NULL,
  `tipe_motor` varchar(50) NOT NULL,
  `foto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kurir`
--

INSERT INTO `kurir` (`kd_kurir`, `username`, `password`, `nm_kurir`, `no_plat`, `tipe_motor`, `foto`) VALUES
('KR002', 'verdy', 'verdy', 'verdy', 'F1234BC', 'BEAT', ''),
('KR003', 'joko', 'joko', 'joko', 'b 123 h', 'Beat', ''),
('KR005', 'epul', 'epul', 'epul', 'F 1216 MP', 'Mio', '');

-- --------------------------------------------------------

--
-- Table structure for table `order_kurir`
--

CREATE TABLE `order_kurir` (
  `invoice` binary(13) NOT NULL,
  `kd_kurir` varchar(20) NOT NULL,
  `status` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_kurir`
--

INSERT INTO `order_kurir` (`invoice`, `kd_kurir`, `status`) VALUES
(0x35633138333461363766633661, 'KR002', 'pencucian'),
(0x35633138613838363632633131, 'KR002', 'pencucian'),
(0x35633138613938373861353037, 'KR002', 'dijemput'),
(0x35633162336131396164653062, 'KR002', 'dijemput'),
(0x35633162343033356431663063, 'KR003', 'dijemput'),
(0x35633162343033356431663063, 'KR003', 'dijemput'),
(0x35633162356235666262306632, 'KR004', 'dijemput'),
(0x35633162356566623831363530, 'KR002', 'dijemput'),
(0x35633162366461363866323536, 'KR002', 'pencucian'),
(0x35633164623462363362646635, 'KR002', 'pencucian'),
(0x35633165313234343164313639, 'KR005', 'dijemput');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE `transaksi` (
  `invoice` binary(13) NOT NULL,
  `id_user` binary(13) NOT NULL,
  `pakaiankg` int(20) DEFAULT NULL,
  `r_satuan` int(30) DEFAULT NULL,
  `b_satuan` int(30) DEFAULT NULL,
  `alamat` varchar(150) NOT NULL,
  `tanggal` date NOT NULL,
  `harga` int(30) NOT NULL,
  `nm_kurir` varchar(30) DEFAULT NULL,
  `username` varchar(30) NOT NULL,
  `order_status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`invoice`, `id_user`, `pakaiankg`, `r_satuan`, `b_satuan`, `alamat`, `tanggal`, `harga`, `nm_kurir`, `username`, `order_status`) VALUES
(0x35633138333461363766633661, 0x35633137326433336131666563, 3, 1, 0, '', '2018-12-27', 32000, 'verdy', 'kere', 'Selesai'),
(0x35633138613838363632633131, 0x35633137326433336131666563, 3, 0, 0, 'cilebut', '2018-12-19', 10000, 'verdy', 'tuti', 'Selesai'),
(0x35633138613938373861353037, 0x35633138613831396231613761, 2, 0, 0, 'bsi', '2018-12-14', 20000, 'verdy', 'admin', 'Selesai'),
(0x35633162366461363866323536, 0x35633137326433336131666563, 3, 2, 4, 'dsffads', '2018-12-20', 94000, 'verdy', 'dwi', 'Selesai'),
(0x35633164623462363362646635, 0x35633137326433336131666563, 1, 0, 2, 'oye', '2018-12-25', 40000, 'verdy', 'dwi', 'Selesai'),
(0x35633165313234343164313639, 0x35633165313138613964363665, 3, 0, 0, 'jl. baru', '2018-03-09', 30000, 'epul', 'amir', 'Selesai');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `nama` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `id_user` binary(13) NOT NULL,
  `email` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`nama`, `username`, `password`, `id_user`, `email`) VALUES
('mei', 'meisy', '18b37b27e50e010870a9b1d19ef98d6b', 0x35633065353533646230306139, 'meisyfaz@yahoo.com'),
('mia', 'miaazm', '5102ecd3d47f6561de70979017b87a80', 0x35633065653735623462366334, 'miaululazmii@gmail.com'),
('dwi', 'dwi', '7aa2602c588c05a93baf10128861aeb9', 0x35633137326433336131666563, 'dwichunong@gmail.com'),
('a', 'a', '0cc175b9c0f1b6a831c399e269772661', 0x35633137636633613564366566, 'a@gmail.com'),
('bsi', 'bsi', 'da878e5c536f8a3892785d0fcdf3015d', 0x35633138353936616233663134, 'bsi@gmail.com'),
('keri', 'keri', '871228b3ecce54d587815f064069fe94', 0x35633138326537393762613334, 'keri@gmail.com'),
('kere', 'kere', '38795a5473cf3f907c909570d6a8eeb6', 0x35633138333262373466326136, 'kere@gmail.com'),
('tuti', 'tuti', '7da0da6bf56eb7dc3f1b10684b7c806e', 0x35633138613831396231613761, 'tuti@gmail.com'),
('amir', 'amir', '63eefbd45d89e8c91f24b609f7539942', 0x35633165313138613964363665, 'amir@amir.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `kurir`
--
ALTER TABLE `kurir`
  ADD PRIMARY KEY (`kd_kurir`);

--
-- Indexes for table `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`invoice`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
