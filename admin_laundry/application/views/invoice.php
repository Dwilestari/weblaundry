<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style_invoice.css ?>' ?>">
<body onload="window.print();">
<?php foreach($invoicenya as $i) { ?>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title"><br>
            <hr>
                <p><img src="<?php echo base_url('assets'); ?>/img/logo.png" ><br></p><hr>
                <h2>Invoice</h2><h3 class="pull-right">Order <?php echo $i->invoice ?></h3>
            </div>
            <div class="row">
                    <div class="col-xs-6">
                       
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Waktu Pemesanan:</strong><br>
                        <?php echo $i->tanggal ?><br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Rincian Biaya</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th><strong></strong></th>
                                    <th><strong></strong></th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                <!-- foreach ($order->lineItems as $line) or some such thing here -->
                                <tr>
                                    <td><b>Nama Pemesan </b></td>
                                    <td>: <?php echo $i->username ?></td>
                                </tr>
                                <tr>
                                    <td><b>Alamat </b> </td>
                                    <td>: <?php echo $i->alamat ?></td>
                                </tr>
                                <tr>
                                    <td><b>Kurir</b></td>
                                    <td>: <?php echo $i->nm_kurir ?></td>
                                </tr>
                                <tr>
                                    <td><b>Jenis Cucian</b>  </td>
                                    <td></td>    
                                </tr>
                                <tr>
                                    <td>    Kiloan (kg)   </td>
                                    <td>: <?php echo $i->pakaiankg ?></td>
                                </tr><tr>
                                    <td>    Bahan Ringan  </td>
                                    <td>: <?php echo $i->r_satuan ?></td>
                                </tr><tr>
                                    <td>    Bahan Berat   </td>
                                    <td>: <?php echo $i->b_satuan ?></td>
                                </tr>
                                <tr >
                                    <td><b>Total        </b> </td>
                                    <td><b>: <?php echo $i->harga ?></b></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

            </div>
            <div class="col-md-6 text-center">
            <b>Kurir</b>
                    <br><br><br><br>
                    <b><?php echo $i->nm_kurir ?></b>
                    </div>
            <div class="col-md-6 text-center">
            <b>Pemesan</b>
                    <br><br><br><br>
                    <b><?php echo $i->username ?></b>
                    </div>
        </div>
    </div>
</div>
<?php } ?>
</body>