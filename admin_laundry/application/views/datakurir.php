<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin - Males Nyuci</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href=" <?php echo base_url ('assets'); ?>/img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a class="navbar-brand"><img src="<?php echo base_url('assets'); ?>/img/logo.png" alt="logo"></a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">
            <!-- <form id="searchForm" class="ml-auto d-none d-lg-block">
              <div class="form-group position-relative mb-0">
                <button type="submit" style="top: -3px; left: 0;" class="position-absolute bg-white border-0 p-0"><i class="o-search-magnify-1 text-gray text-lg"></i></button>
                <input type="search" placeholder="Search ..." class="form-control form-control-sm border-0 no-shadow pl-4">
              </div>
            </form> -->
          </li>
          <li class="nav-item dropdown mr-3"><a id="notifications" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle text-gray-400 px-1"><i class="fa fa-bell"></i><span class="notification-icon"></span></a>
            <div aria-labelledby="notifications" class="dropdown-menu"><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a><a href="#" class="dropdown-item"> 
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-green text-white"><i class="fas fa-envelope"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 6 new messages</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-blue text-white"><i class="fas fa-upload"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">Server rebooted</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item text-center"><small class="font-weight-bold headings-font-family text-uppercase">View all notifications</small></a>
            </div>
          </li>
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="<?php echo base_url ('assets'); ?>/img/avatar-6.jpg" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family"><?php echo $this->session->userdata('nama')  ?></strong></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Settings</a><a href="#" class="dropdown-item">Activity log       </a>
              <div class="dropdown-divider"></div><a href="<?php echo base_url ('Admin'); ?>" class="dropdown-item">Logout</a>
            </div>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">
      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/home'); ?>" class="sidebar-link text-muted"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('C_datakurir'); ?>" class="sidebar-link text-muted active"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Kurir</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/pelanggan'); ?>" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Pelanggan</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/laporan'); ?>" class="sidebar-link text-muted"><i class="o-survey-1 mr-3 text-gray"></i><span>Laporan</span></a></li>
        </ul>
        </div>
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <br>
          <section>
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="text-uppercase mb-0">Kurir</h2>
                  </div>
                  <div class="card-body">                           
                    <table class="table table-striped card-text">
                      <thead>
                        <tr>
                          <th>KODE KURIR</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>NAMA KURIR</th>
                          <th>NO PLAT</th>
                          <th>TIPE MOTOR</th>
                          <th>EDIT</th>
                          <th>DELETE</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php  foreach ($kurir as $k){ ?>
                                        <tr>
                                            <td><?php echo $k->kd_kurir  ?></td>
                                            <td><?php echo $k->username ?></td>
                                            <td><?php echo $k->password ?></td>
                                            <td><?php echo $k->nm_kurir ?></td>
                                            <td><?php echo $k->no_plat ?></td>
                                            <td><?php echo $k->tipe_motor ?></td>
                                            <td>
                                                <button type="button" class='btn btn-primary' data-toggle='modal' data-target='#modalku<?php echo $k->kd_kurir ?>'>EDIT
                                                </button>
                                            </td>
                                            <td> <?php echo anchor('C_datakurir/delete/'.$k->kd_kurir,'DELETE'); ?></td>
                                        </tr>
                          <!-- Modal-->
                                  <div class="modal fade" id="modalku<?php echo $k->kd_kurir ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                          <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <?php echo form_open('C_datakurir/edit_simpan'); ?>
                                              <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit Kurir</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                  <span aria-hidden="true">&times;</span>
                                                </button>
                                              </div>
                                              <div class="modal-body">
                                                <input type="hidden" id="kd_kurir" name="kd_kurir" class="form-control" placeholder="kd_kurir" required="required" autofocus="autofocus" value="<?php echo $k->kd_kurir ?>">
                                                    <div class="form-group">
                                                      <label for="username">Username</label>
                                                          <input type="text" id="inputUsername" name="username" class="form-control" placeholder="username" required="required" value="<?php echo $k->username ?>">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="password">Password</label>
                                                          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="password" required="required"  value="<?php echo $k->password ?>">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="nm_kurir">Nama Kurir</label>
                                                          <input type="text" id="inputNama" name="nm_kurir" class="form-control" placeholder="nm_kurir" required="required" value="<?php echo $k->nm_kurir ?>">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="inputPlat">No Plat</label>
                                                          <input type="text" id="inputPlat" name="no_plat" class="form-control" placeholder="no_plat" required="required" value="<?php echo $k->no_plat ?>">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="tipe_motor">Tipe Motor</label>
                                                          <input type="text" id="inputTipe" name="tipe_motor" class="form-control" placeholder="tipe_motor" required="required" value="<?php echo $k->tipe_motor ?>">
                                                    </div>
                                              </div>
                                              <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                                <button class="btn btn-primary btn-block" type="submit">SIMPAN</button>
                                              </div>
                                          
                                            <?php echo form_close(); ?>
                                        </div>
                                        
                                          </div>
                                        </div>
                                        <?php } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-6 mb-4 mb-lg-0 pl-lg-0">
                <div class="card mb-3">
                  <div class="card-header">
                    <h2 class="mb-0 d-flex align-items-center"><span>Input</span><span class="dot bg-green d-inline-block ml-3"></span></h2><span class="text-muted text-uppercase small">Data Kurir</span>
                  </div>
                  <div class="card-body">
                    <div class="row align-items-center flex-row">
                        <tbody>
                                    <?php echo form_open('C_datakurir/input_simpan');?> 
                                             
                                                <div class="form-group">
                                                  <label for="inputEmail">Kode Kurir</label>
                                                      <input type="text" id="inputEmail" name="kd_kurir" class="form-control" placeholder="Kode Kurir" required="required" autofocus="autofocus">
                                                </div>

                                                <div class="form-group">
                                                      <label for="nm_kurir">Username</label>
                                                          <input type="text" id="inputUsername" name="username" class="form-control" placeholder="Username" required="required" autofocus="autofocus">
                                                    </div>

                                                    <div class="form-group">
                                                      <label for="nm_kurir">Password</label>
                                                          <input type="password" id="inputPassword" name="password" class="form-control" placeholder="Password" required="required" autofocus="autofocus">
                                                    </div>

                                                <div class="form-group">
                                                  <label for="inputPassword">Nama Kurir</label>
                                                      <input type="text" id="inputPassword" name="nm_kurir" class="form-control" placeholder="Nama Kurir" required="required">
                                                </div>

                                                <div class="form-group">
                                                  <label for="inputPassword">No Plat</label>
                                                      <input type="harga" id="inputPassword" name="no_plat" class="form-control" placeholder="No Plat" required="required">
                                                </div>

                                                <div class="form-group">
                                                  <label for="inputPassword">Tipe Motor</label>
                                                      <input type="harga" id="inputPassword" name="tipe_motor" class="form-control" placeholder="Tipe Motor" required="required">
                                                </div>
                                                <button class="btn btn-primary btn-block"type="submit">SIMPAN</button>
                                        
                                    <?php echo form_close();?>
                        </tbody>
                      </div>
                    </div>
                  </div>
                </div>       
          </section>

        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Your company &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by Males Nyuci</p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/popper.js/umd/popper.min.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/charts-home.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/front.js"></script>
  </body>
</html>