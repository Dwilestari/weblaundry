<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin - Males Nyuci</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href=" <?php echo base_url ('assets'); ?>/img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a class="navbar-brand"><img src="<?php echo base_url('assets'); ?>/img/logo.png" alt="logo"></a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">
          </li>
          <li class="nav-item dropdown mr-3"><a id="notifications" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle text-gray-400 px-1"><i class="fa fa-bell"></i><span class="notification-icon"></span></a>
            <div aria-labelledby="notifications" class="dropdown-menu"><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a><a href="#" class="dropdown-item"> 
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-green text-white"><i class="fas fa-envelope"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 6 new messages</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-blue text-white"><i class="fas fa-upload"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">Server rebooted</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item text-center"><small class="font-weight-bold headings-font-family text-uppercase">View all notifications</small></a>
            </div>
          </li>
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="<?php echo base_url ('assets'); ?>/img/avatar-6.jpg" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family"><?php echo $this->session->userdata('nama')  ?></strong></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Settings</a><a href="#" class="dropdown-item">Activity log       </a>
              <div class="dropdown-divider"></div><a href="<?php echo base_url ('Admin'); ?>" class="dropdown-item">Logout</a>
            </div>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">
      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/home'); ?>" class="sidebar-link text-muted"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('C_datakurir'); ?>" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Kurir</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/pelanggan'); ?>" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Pelanggan</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/laporan'); ?>" class="sidebar-link text-muted active"><i class="o-survey-1 mr-3 text-gray"></i><span>Laporan</span></a></li>
        </ul>
        </div>
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <br>
          <!-- <section class="py-5">
            <div class="row">
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-violet"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Order</h6><span class="text-gray">145,14 GB</span>
                    </div>
                  </div>
                  <div class="icon text-white bg-violet"><i class="fas fa-receipt"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-green"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Penjualan</h6><span class="text-gray">32</span>
                    </div>
                  </div>
                  <div class="icon text-white bg-green"><i class="far fa-clipboard"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-blue"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Pelanggan</h6><span class="text-gray">400</span>
                    </div>
                  </div>
                  <div class="icon text-white bg-blue"><i class="fa fa-user"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-red"></div>
                    <div class="text">
                      <h6 class="mb-0">Kurir Online</h6><span class="text-gray">123</span>
                    </div>
                  </div>
                  <div class="icon text-white bg-red"><i class="fa fa-users"></i></div>
                </div>
              </div>
            </div>
          </section> -->
          <section>
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h1 class="h2 text-uppercase mb-0">Laporan</h1>
                  </div>
                  <div class="card-body">                           
                    <div class="form-group row">
                        <div class="col-md-9">
                          <div class="input-group mb-3">
                            <!-- <label class="col-md-2 form-control-label">Pilih Bulan</label>
                            <select name="bulan" class="btn btn-outline-primary dropdown-toggle">
                                <option value="01">Januari</option>
                                <option value="02">Februari</option>
                                <option value="03">Maret</option>
                                <option value="04">April</option>
                                <option value="05">Mei</option>
                                <option value="06">Juni</option>
                                <option value="07">Juli</option>
                                <option value="08">Agustus</option>
                                <option value="09">September</option>
                                <option value="10">Oktober</option>
                                <option value="12">November</option>
                                <option value="12">Desember</option>
                            </select>
                              <button type="submit" class="btn btn-primary">Pilih</button> -->
                             <!--  <?php 
                                $bulan = $_POST['bulan'];
                                $sql = "SELECT * FROM order where month(tanggal)='$bulan' ";
                               ?> -->
                          </div>
                        </div>
                    </div>
                    <br>
                    <table class="table table-striped card-text">
                      <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Pelanggan</th>
                          <th>Alamat</th>
                          <th>Tanggal</th>
                          <th>Pakaian<sup>kg</sup></th>
                          <th>Ringan Satuan</th>
                          <th>Berat Satuan</th>
                          <th>Harga</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($order as $o) {?>
                        <tr>
                          <td><?php echo $o->invoice ?></td>
                          <td><?php echo $o->username ?></td>
                          <td><?php echo $o->alamat ?></td>
                          <td><?php echo $o->tanggal ?></td>
                          <td><?php echo $o->pakaiankg ?></td>
                          <td><?php echo $o->r_satuan ?></td>
                          <td><?php echo $o->b_satuan ?></td>
                          <td><?php echo $o->harga ?></td>
                        </tr>
                      <?php }?>
                      </tbody>
                    </table>
                      <div class="col-md-12 text-md-right">
                          <b>Total  Penghasilan : <?php echo $tot ?></b>
                      </div>
                      <div class="col-md-2 align-items-center">
                          <a target="_blank" href="<?php echo base_url('Admin/cetak_laporan') ?>" class="btn btn-primary btn-block" >Cetak</a>
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Your company &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by Males Nyuci</p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/popper.js/umd/popper.min.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/charts-home.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/front.js"></script>
  </body>
</html>