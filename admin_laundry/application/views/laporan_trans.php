<link href="//netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->
<link rel="stylesheet" type="text/css" href="<?php echo base_url().'assets/css/style_invoice.css ?>' ?>">
<body onload="window.print();">
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title"><br>
            <hr>
                <p><img src="<?php echo base_url('assets'); ?>/img/logo.png" ><br></p><hr>
                <h2>LAPORAN</h2>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Rincian Biaya</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                <table class="table table-striped card-text">
                      <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Pelanggan</th>
                          <th>Alamat</th>
                          <th>Tanggal</th>
                          <th>Pakaian<sup>kg</sup></th>
                          <th>Ringan Satuan</th>
                          <th>Berat Satuan</th>
                          <th>Harga</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($order as $o) {?>
                        <tr>
                          <td><?php echo $o->invoice ?></td>
                          <td><?php echo $o->username ?></td>
                          <td><?php echo $o->alamat ?></td>
                          <td><?php echo $o->tanggal ?></td>
                          <td><?php echo $o->pakaiankg ?></td>
                          <td><?php echo $o->r_satuan ?></td>
                          <td><?php echo $o->b_satuan ?></td>
                          <td><?php echo $o->harga ?></td>
                        </tr>
                      <?php }?>
                      </tbody>
                    </table>
                    <div class="col-md-12 text-md-center">
                        <b>Total  Penghasilan : <?php echo $tot ?></b>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>