<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin - Males Nyuci</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome CSS-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!-- Google fonts - Popppins for copy-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,800">
    <!-- orion icons-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/orionicons.css">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href=" <?php echo base_url ('assets'); ?>/css/custom.css">
    <!-- Favicon-->
    <link rel="shortcut icon" href=" <?php echo base_url ('assets'); ?>/img/favicon.png">
    <!-- Tweaks for older IEs--><!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
  </head>
  <body>
    <!-- navbar-->
    <header class="header">
      <nav class="navbar navbar-expand-lg px-4 py-2 bg-white shadow"><a href="#" class="sidebar-toggler text-gray-500 mr-4 mr-lg-5 lead"><i class="fas fa-align-left"></i></a><a class="navbar-brand"><img src="<?php echo base_url('assets'); ?>/img/logo.png" alt="logo"></a>
        <ul class="ml-auto d-flex align-items-center list-unstyled mb-0">
          <li class="nav-item">
            <!-- <form id="searchForm" class="ml-auto d-none d-lg-block">
              <div class="form-group position-relative mb-0">
                <button type="submit" style="top: -3px; left: 0;" class="position-absolute bg-white border-0 p-0"><i class="o-search-magnify-1 text-gray text-lg"></i></button>
                <input type="search" placeholder="Search ..." class="form-control form-control-sm border-0 no-shadow pl-4">
              </div>
            </form> -->
          </li>
          <li class="nav-item dropdown mr-3"><a id="notifications" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle text-gray-400 px-1"><i class="fa fa-bell"></i><span class="notification-icon"></span></a>
            <div aria-labelledby="notifications" class="dropdown-menu"><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a><a href="#" class="dropdown-item"> 
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-green text-white"><i class="fas fa-envelope"></i></div>
                    <div class="text ml-2">
                      <p class="mb-0">You have 6 new messages</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-blue text-white"><i class="fas fa-upload"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">Server rebooted</p>
                  </div>
                </div></a><a href="#" class="dropdown-item">
                <div class="d-flex align-items-center">
                  <div class="icon icon-sm bg-violet text-white"><i class="fab fa-twitter"></i></div>
                  <div class="text ml-2">
                    <p class="mb-0">You have 2 followers</p>
                  </div>
                </div></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item text-center"><small class="font-weight-bold headings-font-family text-uppercase">View all notifications</small></a>
            </div>
          </li>
          <li class="nav-item dropdown ml-auto"><a id="userInfo" href="http://example.com" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link dropdown-toggle"><img src="<?php echo base_url ('assets'); ?>/img/avatar-6.jpg" alt="Jason Doe" style="max-width: 2.5rem;" class="img-fluid rounded-circle shadow"></a>
            <div aria-labelledby="userInfo" class="dropdown-menu"><a href="#" class="dropdown-item"><strong class="d-block text-uppercase headings-font-family"><?php echo $this->session->userdata('nama')  ?></strong></a>
              <div class="dropdown-divider"></div><a href="#" class="dropdown-item">Settings</a><a href="#" class="dropdown-item">Activity log       </a>
              <div class="dropdown-divider"></div><a href="<?php echo base_url ('Admin'); ?>" class="dropdown-item">Logout</a>
            </div>
          </li>
        </ul>
      </nav>
    </header>
    <div class="d-flex align-items-stretch">
      <div id="sidebar" class="sidebar py-3">
        <div class="text-gray-400 text-uppercase px-3 px-lg-4 py-4 font-weight-bold small headings-font-family">MAIN</div>
        <ul class="sidebar-menu list-unstyled">
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/home'); ?>" class="sidebar-link text-muted active"><i class="o-home-1 mr-3 text-gray"></i><span>Home</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('C_datakurir'); ?>" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Kurir</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/pelanggan'); ?>" class="sidebar-link text-muted"><i class="o-table-content-1 mr-3 text-gray"></i><span>Data Pelanggan</span></a></li>
              <li class="sidebar-list-item"><a href="<?php echo base_url ('Admin/laporan'); ?>" class="sidebar-link text-muted"><i class="o-survey-1 mr-3 text-gray"></i><span>Laporan</span></a></li>
        </ul>
        </div>
      <div class="page-holder w-100 d-flex flex-wrap">
        <div class="container-fluid px-xl-5">
          <section class="py-5">
            <div class="row">
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-violet"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Order</h6><span class="text-gray"><?php echo $total_order ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-violet"><i class="fas fa-receipt"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-green"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Pendapatan</h6><span class="text-gray">RP. <?php echo $total_pendapatan ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-green"><i class="far fa-clipboard"></i></div>
                </div>
              </div>
              <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-blue"></div>
                    <div class="text">
                      <h6 class="mb-0">Total Pelanggan</h6><span class="text-gray"><?php echo $total_pelanggan ?></span>
                    </div>
                  </div>
                  <div class="icon text-white bg-blue"><i class="fa fa-user"></i></div>
                </div>
              </div>
              <!-- <div class="col-xl-3 col-lg-6 mb-4 mb-xl-0">
                <div class="bg-white shadow roundy p-4 h-100 d-flex align-items-center justify-content-between">
                  <div class="flex-grow-1 d-flex align-items-center">
                    <div class="dot mr-3 bg-red"></div>
                    <div class="text">
                      <h6 class="mb-0">Kurir Online</h6><span class="text-gray">123</span>
                    </div>
                  </div>
                  <div class="icon text-white bg-red"><i class="fa fa-users"></i></div>
                </div>
              </div> -->
            </div>
          </section>
          <section>
            <div class="row mb-4">
              <div class="col-lg-12 mb-4 mb-lg-0">
                <div class="card">
                  <div class="card-header">
                    <h2 class="h6 text-uppercase mb-0">Order</h2>
                  </div>
                  <div class="card-body">                           
                    <table class="table table-responsive card-text ">
                      <thead>
                        <tr>
                          <th>Invoice</th>
                          <th>Id Pelanggan</th>
                          <th>Nama Pemesan</th>
                          <th>Alamat</th>
                          <th>Tanggal</th>
                          <th>Pakaian<sup>kg</sup></th>
                          <th>Ringan Satuan</th>
                          <th>Berat Satuan</th>
                          <th>Harga</th>
                          <th>Kurir</th>
                          <th>Aksi</th> 
                          <th>Cetak</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($order as $o) {?>
                        <tr>
                          <td><?php echo $o->invoice ?></td>
                          <td><?php echo $o->id_user ?></td>
                          <td><?php echo $o->username ?></td>
                          <td><?php echo $o->alamat ?></td>
                          <td><?php echo $o->tanggal ?></td>
                          <td><?php echo $o->pakaiankg ?></td>
                          <td><?php echo $o->r_satuan ?></td>
                          <td><?php echo $o->b_satuan ?></td>
                          <td><?php echo $o->harga ?></td>
                          <td>
                            <?php if(!empty($o->nm_kurir)){ ?>
                              <?php echo $o->nm_kurir ?>
                            <?php }else{ ?>
                              <button type="button" data-toggle="modal" data-target="#myModalkurir<?php echo $o->invoice ?>" class="btn btn-primary">Pilih Kurir</button>
                          </td>
                          <?php } ?>
                          <td>
                            <?php if(!empty($o->nm_kurir)){ ?>
                              <?php echo $o->order_status   ?>
                              <button type="button" data-toggle="modal" data-target="#myModalstatus" class="btn btn-primary o-survey-1 mr-3 text-gray"></button>
                            <?php } elseif($o->order_status == 'Selesai') { ?>
                               
                              <?php echo $o->order_status   ?>
                            <?php }else{ ?>
                            <button type="button" data-toggle="modal" data-target="#myModalstatus" class="btn btn-primary o-survey-1 mr-3 text-gray" disabled=""></button>
                          <?php } ?>
                          </td>
                          <td>
                            <?php echo anchor('Admin/invoice/'.$o->invoice,'<i class="o-survey-1"></i>'); ?>
                            <!-- <a href="<?php echo base_url('Admin/invoice')?>" class="fas fa-print "></a> -->
                            
                          </td>
                        </tr>
                      <?php }?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Modal-->
            <?php foreach ($order as $order) { ?>
                <div id="myModalkurir<?php echo $order->invoice ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                  <div role="document" class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="mb-0 d-flex align-items-center"><span>Kurir</span><span class="dot bg-green d-inline-block ml-3"></span></h4>
                        <h4> No. Invoice <?php echo $order->invoice ?></h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                                                   
                            <table class="table table-striped card-text">
                              <thead>
                                <tr>
                                  <th>KODE KURIR</th>
                                  <th>NAMA KURIR</th>
                                  <th>NO PLAT</th>
                                  <th>TIPE MOTOR</th>
                                  <th>Aksi</th>
                                 
                                </tr>
                              </thead>
                              <tbody>
                                <?php  foreach ($kurir as $k){ ?>
                                  <tr>
                                      <td><?php echo $k->kd_kurir  ?></td>
                                      <td><?php echo $k->nm_kurir ?></td> 
                                      <td><?php echo $k->no_plat ?></td>
                                      <td><?php echo $k->tipe_motor ?></td>
                                      <td>
                                        <form action="<?php echo base_url('C_datakurir/order_kurir') ?>" method="post">
                                          <input type="hidden" name="invoice" value="<?php echo $order->invoice ?>">
                                          <input type="hidden" name="kd_kurir" value="<?php echo $k->kd_kurir  ?>">
                                          <input type="hidden" name="nm_kurir" value="<?php echo $k->nm_kurir  ?>">
                                          <button type='submit' class='btn btn-secondary'>Pilih</button>
                                        </form>
                                        </td>
                                    </tr>
                                  <?php } ?>
                              </tbody>
                            </table>
                      </div>
                    </div>
                  </div>
                </div>
                  <?php } ?>

             <!-- Modal Ubah Status-->
            <!-- <?php foreach ($order as $order) { ?> -->
                <div id="myModalstatus" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" class="modal fade text-left">
                  <div role="document" class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h4 class="mb-0 d-flex align-items-center"><span>Kurir</span><span class="dot bg-green d-inline-block ml-3"></span></h4>
                        <h4> Ubah Status Order</h4>
                        <button type="button" data-dismiss="modal" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                      </div>
                      <div class="modal-body">
                                                   
                            <!-- <table class="table table-striped card-text"> -->
                            <form action="<?php echo base_url('Admin/ubahstatus') ?>" method="post">
                            <select name="status" class="btn btn-outline-primary dropdown-toggle">
                                <option value="Sedang dicuci">Sedang dicuci</option>
                                <option value="Sedang dipacking">Sedang dipacking</option>
                                <option value="Sedang dikirim">Sedang dikirim</option>
                                <option value="Selesai">Selesai</option>
                            </select>
                                          <!-- <input type="hidden" name="invoice" value="<?php echo $order->invoice ?>"> -->
                             <button type="submit" class="btn btn-primary">Ubah</button>
                           </form>
                              <!-- <thead>
                                <tr>
                                  <th>KODE KURIR</th>
                                  <th>NAMA KURIR</th>
                                  <th>NO PLAT</th>
                                  <th>TIPE MOTOR</th>
                                  <th>Aksi</th>
                                 
                                </tr>
                              </thead>
                              <tbody>
                                <?php  foreach ($kurir as $k){ ?>
                                                <tr>
                                                    <td><?php echo $k->kd_kurir  ?></td>
                                                    <td><?php echo $k->nm_kurir ?></td> 
                                                    <td><?php echo $k->no_plat ?></td>
                                                    <td><?php echo $k->tipe_motor ?></td>
                                                    <td>
                                                      <form action="<?php echo base_url('C_datakurir/order_kurir') ?>" method="post">
                                                        <input type="hidden" name="invoice" value="<?php echo $order->invoice ?>">
                                                        <input type="hidden" name="kd_kurir" value="<?php echo $k->kd_kurir  ?>">
                                                        <input type="hidden" name="nm_kurir" value="<?php echo $k->nm_kurir  ?>">
                                                        <button type='submit' class='btn btn-secondary'>Pilih</button>
                                                      </form>
                                                      </td>
                                                  </tr>
                                                <?php } ?>
                              </tbody> -->
                            <!-- </table> -->
                      </div>
                    </div>
                  </div>
                </div>
                  <!-- <?php } ?> -->
          </section>
        </div>
        <footer class="footer bg-white shadow align-self-end py-3 px-xl-5 w-100">
          <div class="container-fluid">
            <div class="row">
              <div class="col-md-6 text-center text-md-left text-primary">
                <p class="mb-2 mb-md-0">Your company &copy; 2018-2020</p>
              </div>
              <div class="col-md-6 text-center text-md-right text-gray-400">
                <p class="mb-0">Design by Males Nyuci</p>
                <!-- Please do not remove the backlink to us unless you support further theme's development at https://bootstrapious.com/donate. It is part of the license conditions. Thank you for understanding :)-->
              </div>
            </div>
          </div>
        </footer>
      </div>
    </div>
    <!-- JavaScript files-->
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery/jquery.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/popper.js/umd/popper.min.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/jquery.cookie/jquery.cookie.js"> </script>
    <script src=" <?php echo base_url ('assets'); ?>/vendor/chart.js/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/js-cookie@2/src/js.cookie.min.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/charts-home.js"></script>
    <script src=" <?php echo base_url ('assets'); ?>/js/front.js"></script>
  </body>
</html>