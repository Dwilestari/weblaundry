<?php 

class M_login extends CI_Model
{
	
	public function proses_login($data){
			 $this->db->where($data);
	 $user= $this->db->get('admin');
	 return $user;
	}	

	public function data_order(){
		return $this->db->get('transaksi');
	}

	public function keinvoice($where,$table){
		return $this->db->get_where($table,$where);
	}

	public function ubah_status(){
		$data = array(
			'order_status' => $this->input->post('status')
		);
		$this->db->where(array('id_user' => $this->session->userdata('id')));
		$this->db->update('transaksi', $data);
	}

	function list_user(){
	    //ambil data barang dari tabel barang
	    $user = $this->db->get('user');
	    return $user;
	}

	public function total_order(){
	$query = $this->db->get('transaksi');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}

	public function total_pelanggan(){
	$query = $this->db->get('kurir');
		if($query->num_rows()>0)
		{
		return $query->num_rows();
		}
		else
		{
		return 0;
		}
	}
}
