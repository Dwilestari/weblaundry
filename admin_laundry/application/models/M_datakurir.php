<?php

class M_datakurir extends CI_Model{

	 function list_kurir(){
        //ambil data barang dari tabel barang
        $kurir = $this->db->get('kurir');
        return $kurir;
    	}

          function product($kd_kurir){
        return $this->db->get_where('kurir',array('kd_kurir'=>$kd_kurir));
  		}

      function order_kurir($data){
        $kd_kurir=$this->input->post('kd_kurir');
        $this->db->where(array('kd_kurir'=>$kd_kurir));
        $this->db->insert('order_kurir', $data);
      }
      public function update_order()
      {
        $nm_kurir=$this->input->post('nm_kurir');
        $invoice=$this->input->post('invoice');
        $this->db->where(array('invoice' => $invoice));
        $this->db->update('transaksi', array('nm_kurir' => $nm_kurir));

      }

  		public function update()
  		{

         $kd_kurir = $this->input->post('kd_kurir');
         $datakurir = array(
            'nm_kurir'   => $this->input->post('nm_kurir'),
            'no_plat'  => $this->input->post('no_plat'),
            'tipe_motor'  => $this->input->post('tipe_motor')
        );
        	$this->db->where(array('kd_kurir' => $kd_kurir));
        	$this->db->update('kurir',$datakurir);
         redirect ('C_datakurir');
  		}

      public function total_order(){
        $this->db->select('SUM(harga) as total');
        $this->db->from('transaksi');
        return $this->db->get()->row()->total;
      }

}