<?php 

class Admin extends CI_controller{

public function __construct(){
		parent::__construct();
		$this->load->model('M_login');
        $this->load->model('M_datakurir');

		if($this->session->userdata('nama') || $this->session->userdata('username')){
		
	}
}

public function index (){
	$this->load->view('login');
}

// public function login(){
// 	$this->load->view('login'); 
// }

public function home (){
	$data['total_pendapatan'] = $this->M_datakurir->total_order();
	$data['total_pelanggan'] = $this->M_login->total_pelanggan();
	$data['total_order'] = $this->M_login->total_order();
	$data['order'] = $this->M_login->data_order()->result();
    $data['kurir'] = $this->M_datakurir->list_kurir()->result();
	$this->load->view('home', $data);
}

public function ubahstatus(){

	$this->M_login->ubah_status();
	redirect(base_url('Admin/home'));
}
public function pelanggan (){
	$data['user'] = $this->M_login->list_user()->result();
	$this->load->view('datapelanggan',$data);
}

public function invoice($primary){
	$where = array('invoice' => $primary);
	$data['invoicenya'] = $this->M_login->keinvoice($where,'transaksi')->result();
	$this->load->view('invoice',$data);
}

public function laporan (){
	$data['tot']	= $this->M_datakurir->total_order();
	$data['order'] = $this->M_login->data_order()->result();
	$this->load->view('laporan',$data);
}
public function cetak_laporan(){
	$data['tot']	= $this->M_datakurir->total_order();
	$data['order'] = $this->M_login->data_order()->result();
	$this->load->view('laporan_trans',$data);
}

public function proses_login(){
		
		$data=array(
		'username'	=> $this->input->post('username'),
		'password'	=> md5($this->input->post('password')),
		);
		
		$user=$this->M_login->proses_login($data);
		if($user->num_rows() > 0){
			//berhasil
			$data_login=$user->row_array();
			$data_sesi=array(
			'nama'		=> $data_login['nama'],
			'username'	=> $data_login['username'],
			);
			$this->session->set_userdata($data_sesi);

			//echo print_r($this->session->userdata());
			redirect (base_url('Admin/home'));

		} else{
			//gagal
			echo "GAGAL LOGIN";
		}
	}
}
