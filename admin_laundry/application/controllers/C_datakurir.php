<?php
class C_datakurir extends CI_Controller{

    public function __construct(){
        parent::__construct();
        

        if($this->session->userdata('nama') || $this->session->userdata('username')){
        
        }
        $this->load->model('M_datakurir');
    }
 
    function index(){
        $judul          = "Daftar Paket";
        $data['judul']  =$judul;
        $data['kurir'] = $this->M_datakurir->list_kurir()->result();
        $this->load->view('datakurir',$data);
      }

      function order_kurir(){
        $invoice=$this->input->post('invoice');
        $kd_kurir=$this->input->post('kd_kurir');
        $data=array(
            'invoice'=> $invoice,
            'kd_kurir'=>$kd_kurir,
            'status' => 'dijemput'
            
        );
        $this->M_datakurir->order_kurir($data);
        $this->M_datakurir->update_order($data);
        redirect(base_url('Admin/home'));
      }

    function input(){
        $this->load->view ('input_kurir');
    }

    function input_simpan(){
        $datakurir = array(
            'kd_kurir'     =>$this->input->post('kd_kurir'),
            'username'     =>$this->input->post('username'),
            'password'     =>$this->input->post('password'),
            'nm_kurir'   =>$this->input->post('nm_kurir'),
            'no_plat'  =>$this->input->post('no_plat'),
            'tipe_motor'  =>$this->input->post('tipe_motor'));
            
        $this->db->insert('kurir',$datakurir);
        redirect ('C_datakurir');
    }

    function edit(){
        $this->load->model('M_datakurir');
        $kd_kurir = $this->uri->segment(3);
        $data ['product'] = $this->model_kurir->product($kd_kurir)->row_array();
        $this->load->view('edit_kurir',$data);
      
    }

    function edit_simpan(){
        $this->M_datakurir->update();
    }

    function delete(){
        $kd_kurir   = $this->uri->segment(3);
        $this->db->where('kd_kurir',$kd_kurir);
        $this->db->delete('kurir');
         redirect ('C_datakurir');
    }
}